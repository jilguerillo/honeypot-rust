# Honeypot-rust

Honeypot-rust es un proyecto que tiene como objetivo crear un honeypot en Rust, donde se registran las direcciones IP de los visitantes. Es útil para detectar y analizar posibles ataques de hackers y otros intentos de intrusión en un sistema.

<img src="https://gitlab.com/jilguerillo/honeypot-rust/uploads/0461af726e9f410bf211c3a48fa6b397/Screenshot_2023-04-20_at_21-50-07_Screenshot.png" >

## Instalación

Para instalar y ejecutar el proyecto Honeypot-rust, sigue estos pasos:

1. Descarga o clona el repositorio en tu sistema local.
2. Asegúrate de tener Rust instalado en tu sistema.
3. Ejecuta el comando `cargo run` en la carpeta del proyecto para compilar y ejecutar la aplicación.

## Uso

Una vez que la aplicación está en ejecución, se creará un servidor web en el puerto 80. Cualquier visita a este servidor registrará la dirección IP del visitante en la consola.

Para detener la aplicación, presiona `Ctrl + C` en la consola.

## Contribución

Si deseas contribuir al proyecto Honeypot-rust, sigue estos pasos:

1. Haz un fork del repositorio en tu cuenta de GitHub.
2. Clona tu fork en tu sistema local.
3. Crea una nueva rama para tus cambios (`git checkout -b mi-nueva-funcionalidad`).
4. Realiza los cambios necesarios y asegúrate de que las pruebas pasen.
5. Haz commit de tus cambios (`git commit -am 'Agrego mi nueva funcionalidad'`).
6. Sube los cambios a tu fork (`git push origin mi-nueva-funcionalidad`).
7. Crea un nuevo Pull Request en el repositorio original.

## Licencia

Este proyecto está bajo la licencia GNU AFFERO GENERAL PUBLIC LICENSE version 3. Consulta el archivo LICENSE para obtener más detalles.
