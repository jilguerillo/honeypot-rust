use chrono::{DateTime, Local};
use std::io::{Read, Write};
use std::net::TcpListener;
use std::time::{SystemTime, UNIX_EPOCH};

fn main() {
    let listener = TcpListener::bind("0.0.0.0:80").unwrap();
    let address = listener.local_addr().unwrap();

    println!("Honeypot listening on http://{}", address);

    for stream in listener.incoming() {
        let mut buffer = [0; 1024];
        let mut stream = stream.unwrap();

        stream.read(&mut buffer).unwrap();

        // Extraer la dirección IP del visitante
        let addr = stream.peer_addr().unwrap().ip();
        println!("Dirección IP del visitante: {}", addr);

        // Extraer el UserAgent del header HTTP
        let user_agent = get_header_value(&buffer, "User-Agent");
        let uset_agent_copy = user_agent.clone();
        println!("User-Agent: {:?}", user_agent);

        // Extraer el Referer del header HTTP
        let referer = get_header_value(&buffer, "Referer");
        let referer_copy = referer.clone();
        if let Some(referer) = referer {
            println!("Referer: {}", referer);
        }

        // Agregar tiempo de conexión
        let timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs();
        let datetime =
            DateTime::<Local>::from(UNIX_EPOCH + std::time::Duration::from_secs(timestamp));
        let formatted_datetime = datetime.format("%d:%m:%Y %H:%M:%S").to_string();
        println!("Time: {}", formatted_datetime);

        // Enviar la respuesta HTTP con la información del visitante y el Referer
        let response = format!("HTTP/1.1 200 OK\r\n\r\nHoneypot-rust\r\n\nIP address: {}\r\nUser-Agent: {}\r\nReferer: {}\r\nTime: {}\r\n", addr, uset_agent_copy.unwrap_or_default(), referer_copy.unwrap_or_default(), formatted_datetime);
        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
    }
}

// Función para extraer el valor de un header HTTP del buffer
fn get_header_value(buffer: &[u8], header: &str) -> Option<String> {
    let header = header.to_lowercase();
    let headers = String::from_utf8_lossy(buffer);

    for line in headers.lines() {
        let parts: Vec<&str> = line.splitn(2, ": ").collect();
        if parts.len() == 2 && parts[0].to_lowercase() == header {
            return Some(parts[1].to_owned());
        }
    }

    None
}
